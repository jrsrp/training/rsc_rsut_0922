
## How to get started

The rustup script will make `rustc`, the compiler; and `cargo` build system and package manager available.


```bash
$ curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```


Starting a new project is very easy:

```bash
$ cargo new hello_world
```

## Project structure

```bash
$ exa -tree hello_world

hello_world
├── Cargo.toml
└── src
   └── main.rs
```


The project structure is very simple. `Cargo.toml` stores the project metadata  and the list of dependencies.

```toml
[package]
name = "hello_world"
version = "0.1.0"
edition = "2018"

[dependencies]
```
In simple project all the code will be stored in `main.rs`. The one created by cargo looks like:

```rust
fn main() {
    println!("Hello, world!");
}
```


`cargo` can then be used to build (`cargo build`) the code or run it (`cargo run`). It will grab all the dependecies and compile the code; it uses [incremental compilation](https://rust-lang.github.io/rfcs/1298-incremental-compilation.html) to improve build times when making small edits.