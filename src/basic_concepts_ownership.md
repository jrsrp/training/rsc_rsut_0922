# Ownership

 It is Rust’s most unique feature, and it enables Rust to make memory safety guarantees without needing a garbage collector.

 All programing languages have to manage the way they use a computer’s memory while running. Some  have **garbage collection** that constantly looks for no longer used memory as the program runs; others, require the programmer to **manually allocate and free the memory**. Rust uses a third approach: memory is managed through a system of ownership. 

- Ownership rules:
  - Each value in memory has a variable that’s called its owner.
  - There can only be one owner at a time.
  - Every variable has a single owning scope. 
  - When the owner goes out of scope, the value will be dropped (i.e variables are in charge of freeing their own resources).

```rust, editable

fn main(){
    {
        let val = 1;
    }
    println!("{}", val);

}
```

