# Iterators:

```rust, editabe
fn main(){
    let input = vec![0,1,2,3,4,5];
    let res: i32 = input.iter()
                .map(|&i| i * i)
                .sum();
    println!("{}", res);

}
```

in parallel:

```rust, editabe
use rayon::prelude::*;
fn main() {
    let input = vec![0,1,2,3,4,5];
    let res: i32 = input.par_iter() 
         .map(|&i| i * i)
         .sum();
    println!("{}", res);
}
```

