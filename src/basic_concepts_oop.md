# OOP

There are no classes and method inheritance.

 Rust uses struct (allows us to combine data items of different types), traits ( functionality a particular type has and can share with other types ) and impl ( used to define implementations on types).

```rust, editable
struct Example {
    number: i32,
}

struct OtherExample {
    number: i32,
}


impl Example {
    fn boo() {
        println!("boo! Example::boo() was called!");
    }

    fn answer(&mut self) {
        self.number += 42;
    }

    fn get_number(&self) -> i32 {
        self.number
    }
}

trait Lazy {
    fn do_nothing(&self);
}

impl Lazy for Example {
    fn do_nothing(&self) {
        println!("doing nothing! also, number is {}!", self.number);
    }
}

impl Lazy for OtherExample {
    fn do_nothing(&self) {
        println!("I'm also lazy! btw, number is {}!", self.number);
    }
}

fn main(){
    Example::boo();
    let e = Example {number: 0};
    e.do_nothing();
    // let z:i32 = 32;
    // z.do_nothing(); // need to implement Lazy for i32 for this to work
    let o = OtherExample {number: 1};
    o.do_nothing();
}

```
