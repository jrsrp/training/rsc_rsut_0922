
# Create a RasterDataset from a single file:

```rust, editable
use eorst::{DataSourceBuilder, RasterDatasetBuilder};
use std::path::PathBuf;

fn main() {
    let data_source =
        DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
            .build();
    let rds = RasterDatasetBuilder::from_source(&data_source).build();
    println!("{rds}");
}

```

