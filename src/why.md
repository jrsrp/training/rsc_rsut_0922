# Why this workshop?

- If we keep working the same way we did for the past 15 years, we will lose relevance.

- We need to experiment with
    - the way we access our data (data sources, file systems)
    - the way we make our code available (containers)
    - how and where we process (worflow managers, HPC, Cloud)
    - and maybe programing languages

- Idea: Rust is a great option! 
    - As 99% of my ideas this one could also be VERY WRONG, but the goal is not to be right but to make us think about the options we have.