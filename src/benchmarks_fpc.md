# Some benchmarks
## minFPC


Calculates NDVI on the fly, take the 2nd smallest value of the time series, and transforms to an FPC index.

- python : https://gitlab.com/jrsrp/themes/woody/fpc/fpcminndvi ~ 36 hours (Denham com. pers.)
- rust : [code](https://gitlab.com/rscdata_science/min_fpc_rs/) (1 hours [24 cores]; 0.52 hours [48 cores]; 0.35 hours [72 cores])

