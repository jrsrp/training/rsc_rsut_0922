# Functions

Start with fn, have a set of parameter after the function name, then curly brackets to tell where the function  begins and ends.

```rust, editable

fn get_message(name: String) -> String {
    format!("hello {}!", name)
}

fn main(){
    let message: String = get_message(String::from("Leo"));
    println!("{}", message);
}

```
