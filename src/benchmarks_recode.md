# Some benchmarks
## recode

Recode a categorical QLD-wide mosaic.

- python : Collett, 360 minuted [1 core]; bottleneck is dictionary lockup.
- rust: (3 minutes [32 cores]; 92 minutes [1 core])