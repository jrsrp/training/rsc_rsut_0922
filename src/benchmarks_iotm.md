Some benchmarks
## temporal composition

- Image of the mont QLD for September; [code](https://gitlab.com/leo_hardtke/iotm_0921)

Frequency of clouds over queensland; starting from individual tile/dates (230 * 73 = 16.790 images). 

<img src="sept-iotm-large.jpg" alt="IOTM" width="350"/>


![Benchmarks](bench.PNG)
