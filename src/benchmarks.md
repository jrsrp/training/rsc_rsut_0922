# Some benchmarks

- [Spatial Biocondition](./benchmarks_sbc.md)
- [min fpc](./benchmarks_fpc.md)
- [recode](./benchmarks_recode.md)
- [temporal composition](./benchmarks_iotm.md)
