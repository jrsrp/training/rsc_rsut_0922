
# Basic concept

- [Mutability](./basic_concepts_mut.md)
- [Data types](./basic_concepts_types.md)
- [Functions](./basic_concepts_fun.md)
- [Ownership](./basic_concepts_ownership.md)
- [References and borrows](./basic_concepts_borrow.md)
- [OOP?](./basic_concepts_oop.md)
- [enum](./basic_concepts_enum.md)
- [iterators](./basic_concepts_iter.md)