# Some benchmarks

## Spatial biocondition:

Samples 17 state wide raster (mean of a 90x90 window around 25.000 points), split cal/val dataset, fit LightGBM, predict state wide.

- python: An early implementation took more than 30 hours just for the prediction.

- rust: Takes less than an hour using 12 cpus. [code](https://gitlab.com/jrsrp/themes/biocondition/biocondition_modelling_framework)
