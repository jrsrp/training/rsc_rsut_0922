# Data types

Every value in Rust is of a certain data type, which tells Rust what kind of data is being specified so it knows how to work with that data.

```rust, editable
fn main() {
    let var = "256".parse().expect("Not a valid number!");
    println!("Var is: {}", var);

}
```

Casting for mathematical operations is not done automatically
```rust, editable
fn main() {
    let a: u8 = 1; 
    let b: f32 = 1.;
    let var = a + b;
    println!("Var is: {}", var);

}
```

