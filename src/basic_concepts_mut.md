
# Variable mutability:

```rust, editable
fn main() {
    let var = 10;
    var = 12;
    println!("Var is: {}", var);
}
```

