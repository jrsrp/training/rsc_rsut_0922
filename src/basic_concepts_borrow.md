# References and borrows

```rust, editable
fn calculate_length(s: String) -> usize {
    s.len()
}

fn main() {
    let s1 = String::from("hello");
    let len = calculate_length(s1); // s1 will be moved to the scope of calculate_length
    println!("The length of '{}' is {}.", s1, len);

}
```
