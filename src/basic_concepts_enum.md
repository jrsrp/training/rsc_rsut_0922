# enum

Data type to store posible options:

```rust, editable

fn main(){
    enum Directions {
        Left,
        Right,
        Up,
    }

    let direction = Directions::Left;
    match direction {
        Directions::Left => {println!{ "Going left!"}},
        Directions::Right => {println!{ "Going right!"}},
    }
}
```


