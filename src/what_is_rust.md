# Why RUST?


![image](main.png)


Also:
    - great community! 
    - structure makes it easy to understand someone elses code.


## Some context 


- Started as a Mozilla project.
- First stable release in 2015.
- Now stewarded by the [Rust fundation](https://foundation.rust-lang.org/).  Composed of 5 directors from our Founding member companies, **AWS**, **Huawei**, **Google**, **Microsoft**, and **Mozilla**, as well as 5 directors from project leadership, 2 representing the Core Team, as well as 3 project areas: Reliability, Quality, and Collaboration.
- In 2021 earned the top spot as the "most-loved" programming language for the fifth consecutive year of Stack Overflow’s developer survey.
- Only other (than C) language (likely to be) used in the Linux kernel. [Rust for linux](https://github.com/Rust-for-Linux/linux)